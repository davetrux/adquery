//Copyright (c) 2012 David Truxall

using System;
using System.Collections.Generic;
using System.Windows.Forms;


namespace AD_Query
{
    /// <summary>
    /// Summary description for Form1.
    /// </summary>
    public class MainForm : System.Windows.Forms.Form, IView
    {
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox LdapString;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox UserName;
        private System.Windows.Forms.TextBox Password;
        private System.Windows.Forms.RadioButton Custom;
        private System.Windows.Forms.RadioButton LoggedIn;
        private System.Windows.Forms.TextBox User;
        private System.Windows.Forms.TextBox Pwd;
        private System.Windows.Forms.Button Test;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox SAMAccount;
        private System.Windows.Forms.CheckBox Email;
        private System.Windows.Forms.CheckBox DisplayName;
        private System.Windows.Forms.CheckBox GUID;
        private System.Windows.Forms.CheckBox Custom1;
        private System.Windows.Forms.TextBox CustomName1;
        private System.Windows.Forms.CheckBox Custom2;
        private System.Windows.Forms.TextBox CustomName2;
        private System.Windows.Forms.TextBox Filter;
        private System.Windows.Forms.Label label4;
        private Button GetSchema;
        private TabControl Tabs;
        private TabPage tabPage1;
        private TabPage tabPage2;
        private CheckBox SaveToFile;
        private Button SchemaElements;
        private TextBox QueryUserName;
        private Label label5;
        private DataGridView Results;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;
        private Label label6;
        private TextBox Host;
        private TextBox DomainName;
        private Label label7;
        protected Controller controller;

        /// <summary>
        /// Constructor
        /// </summary>
        public MainForm()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            this.controller = new Controller(this);
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.LdapString = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.DomainName = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.Pwd = new System.Windows.Forms.TextBox();
            this.User = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Custom = new System.Windows.Forms.RadioButton();
            this.LoggedIn = new System.Windows.Forms.RadioButton();
            this.Password = new System.Windows.Forms.TextBox();
            this.UserName = new System.Windows.Forms.TextBox();
            this.Test = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.CustomName2 = new System.Windows.Forms.TextBox();
            this.Custom2 = new System.Windows.Forms.CheckBox();
            this.CustomName1 = new System.Windows.Forms.TextBox();
            this.Custom1 = new System.Windows.Forms.CheckBox();
            this.GUID = new System.Windows.Forms.CheckBox();
            this.DisplayName = new System.Windows.Forms.CheckBox();
            this.Email = new System.Windows.Forms.CheckBox();
            this.SAMAccount = new System.Windows.Forms.CheckBox();
            this.Filter = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.GetSchema = new System.Windows.Forms.Button();
            this.Tabs = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.SaveToFile = new System.Windows.Forms.CheckBox();
            this.SchemaElements = new System.Windows.Forms.Button();
            this.QueryUserName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Results = new System.Windows.Forms.DataGridView();
            this.label6 = new System.Windows.Forms.Label();
            this.Host = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.Tabs.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Results)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(22, 114);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "LDAP String";
            // 
            // LdapString
            // 
            this.LdapString.Location = new System.Drawing.Point(91, 112);
            this.LdapString.Name = "LdapString";
            this.LdapString.Size = new System.Drawing.Size(553, 20);
            this.LdapString.TabIndex = 1;
            this.LdapString.Text = "DC=mobile,DC=compuware,DC=com";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.DomainName);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.Pwd);
            this.groupBox1.Controls.Add(this.User);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.Custom);
            this.groupBox1.Controls.Add(this.LoggedIn);
            this.groupBox1.Location = new System.Drawing.Point(24, 16);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(620, 64);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Credentials";
            // 
            // DomainName
            // 
            this.DomainName.Location = new System.Drawing.Point(264, 32);
            this.DomainName.Name = "DomainName";
            this.DomainName.Size = new System.Drawing.Size(79, 20);
            this.DomainName.TabIndex = 7;
            this.DomainName.Text = "mobile";
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(264, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 23);
            this.label7.TabIndex = 6;
            this.label7.Text = "Domain";
            // 
            // Pwd
            // 
            this.Pwd.Enabled = false;
            this.Pwd.Location = new System.Drawing.Point(448, 32);
            this.Pwd.Name = "Pwd";
            this.Pwd.Size = new System.Drawing.Size(103, 20);
            this.Pwd.TabIndex = 5;
            this.Pwd.Text = "pass@word1";
            // 
            // User
            // 
            this.User.Enabled = false;
            this.User.Location = new System.Drawing.Point(349, 32);
            this.User.Name = "User";
            this.User.Size = new System.Drawing.Size(93, 20);
            this.User.TabIndex = 4;
            this.User.Text = "dellServer";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(448, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 23);
            this.label3.TabIndex = 3;
            this.label3.Text = "Password";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(349, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 23);
            this.label2.TabIndex = 2;
            this.label2.Text = "Username";
            // 
            // Custom
            // 
            this.Custom.Location = new System.Drawing.Point(141, 27);
            this.Custom.Name = "Custom";
            this.Custom.Size = new System.Drawing.Size(135, 24);
            this.Custom.TabIndex = 1;
            this.Custom.Text = "Custom Credentials:";
            // 
            // LoggedIn
            // 
            this.LoggedIn.Checked = true;
            this.LoggedIn.Location = new System.Drawing.Point(22, 27);
            this.LoggedIn.Name = "LoggedIn";
            this.LoggedIn.Size = new System.Drawing.Size(104, 24);
            this.LoggedIn.TabIndex = 0;
            this.LoggedIn.TabStop = true;
            this.LoggedIn.Text = "Logged In User";
            this.LoggedIn.CheckedChanged += new System.EventHandler(this.LoggedIn_CheckedChanged);
            // 
            // Password
            // 
            this.Password.Location = new System.Drawing.Point(0, 0);
            this.Password.Name = "Password";
            this.Password.Size = new System.Drawing.Size(100, 20);
            this.Password.TabIndex = 0;
            // 
            // UserName
            // 
            this.UserName.Location = new System.Drawing.Point(0, 0);
            this.UserName.Name = "UserName";
            this.UserName.Size = new System.Drawing.Size(100, 20);
            this.UserName.TabIndex = 0;
            // 
            // Test
            // 
            this.Test.Location = new System.Drawing.Point(472, 6);
            this.Test.Name = "Test";
            this.Test.Size = new System.Drawing.Size(104, 23);
            this.Test.TabIndex = 4;
            this.Test.Text = "Test Query";
            this.Test.Click += new System.EventHandler(this.Test_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.CustomName2);
            this.groupBox2.Controls.Add(this.Custom2);
            this.groupBox2.Controls.Add(this.CustomName1);
            this.groupBox2.Controls.Add(this.Custom1);
            this.groupBox2.Controls.Add(this.GUID);
            this.groupBox2.Controls.Add(this.DisplayName);
            this.groupBox2.Controls.Add(this.Email);
            this.groupBox2.Controls.Add(this.SAMAccount);
            this.groupBox2.Location = new System.Drawing.Point(16, 36);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(560, 100);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "AD Fields";
            // 
            // CustomName2
            // 
            this.CustomName2.Location = new System.Drawing.Point(248, 64);
            this.CustomName2.Name = "CustomName2";
            this.CustomName2.Size = new System.Drawing.Size(168, 20);
            this.CustomName2.TabIndex = 7;
            // 
            // Custom2
            // 
            this.Custom2.Location = new System.Drawing.Point(152, 64);
            this.Custom2.Name = "Custom2";
            this.Custom2.Size = new System.Drawing.Size(104, 24);
            this.Custom2.TabIndex = 6;
            this.Custom2.Text = "Custom Field:";
            // 
            // CustomName1
            // 
            this.CustomName1.Location = new System.Drawing.Point(248, 40);
            this.CustomName1.Name = "CustomName1";
            this.CustomName1.Size = new System.Drawing.Size(168, 20);
            this.CustomName1.TabIndex = 5;
            this.CustomName1.Text = "telephoneNumber";
            // 
            // Custom1
            // 
            this.Custom1.Location = new System.Drawing.Point(152, 40);
            this.Custom1.Name = "Custom1";
            this.Custom1.Size = new System.Drawing.Size(104, 24);
            this.Custom1.TabIndex = 4;
            this.Custom1.Text = "Custom Field:";
            // 
            // GUID
            // 
            this.GUID.Location = new System.Drawing.Point(152, 16);
            this.GUID.Name = "GUID";
            this.GUID.Size = new System.Drawing.Size(104, 24);
            this.GUID.TabIndex = 3;
            this.GUID.Text = "GUID";
            // 
            // DisplayName
            // 
            this.DisplayName.Location = new System.Drawing.Point(16, 64);
            this.DisplayName.Name = "DisplayName";
            this.DisplayName.Size = new System.Drawing.Size(104, 24);
            this.DisplayName.TabIndex = 2;
            this.DisplayName.Text = "Display Name";
            // 
            // Email
            // 
            this.Email.Location = new System.Drawing.Point(16, 40);
            this.Email.Name = "Email";
            this.Email.Size = new System.Drawing.Size(104, 24);
            this.Email.TabIndex = 1;
            this.Email.Text = "Email Address";
            // 
            // SAMAccount
            // 
            this.SAMAccount.Checked = true;
            this.SAMAccount.CheckState = System.Windows.Forms.CheckState.Checked;
            this.SAMAccount.Enabled = false;
            this.SAMAccount.Location = new System.Drawing.Point(16, 16);
            this.SAMAccount.Name = "SAMAccount";
            this.SAMAccount.Size = new System.Drawing.Size(136, 24);
            this.SAMAccount.TabIndex = 0;
            this.SAMAccount.Text = "SAM Account Name";
            // 
            // Filter
            // 
            this.Filter.Location = new System.Drawing.Point(53, 8);
            this.Filter.Name = "Filter";
            this.Filter.Size = new System.Drawing.Size(396, 20);
            this.Filter.TabIndex = 6;
            this.Filter.Text = "(objectClass=User)";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(13, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 25);
            this.label4.TabIndex = 7;
            this.label4.Text = "Filter";
            // 
            // GetSchema
            // 
            this.GetSchema.Location = new System.Drawing.Point(292, 15);
            this.GetSchema.Name = "GetSchema";
            this.GetSchema.Size = new System.Drawing.Size(104, 23);
            this.GetSchema.TabIndex = 8;
            this.GetSchema.Text = "Get User Data";
            this.GetSchema.UseVisualStyleBackColor = true;
            this.GetSchema.Click += new System.EventHandler(this.GetSchema_Click);
            // 
            // Tabs
            // 
            this.Tabs.Controls.Add(this.tabPage1);
            this.Tabs.Controls.Add(this.tabPage2);
            this.Tabs.Location = new System.Drawing.Point(24, 141);
            this.Tabs.Name = "Tabs";
            this.Tabs.SelectedIndex = 0;
            this.Tabs.Size = new System.Drawing.Size(624, 169);
            this.Tabs.TabIndex = 9;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.Filter);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.Test);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(616, 143);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Search";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.SaveToFile);
            this.tabPage2.Controls.Add(this.SchemaElements);
            this.tabPage2.Controls.Add(this.QueryUserName);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.GetSchema);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(616, 143);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Schema";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // SaveToFile
            // 
            this.SaveToFile.AutoSize = true;
            this.SaveToFile.Location = new System.Drawing.Point(80, 54);
            this.SaveToFile.Name = "SaveToFile";
            this.SaveToFile.Size = new System.Drawing.Size(96, 17);
            this.SaveToFile.TabIndex = 12;
            this.SaveToFile.Text = "Save To A File";
            this.SaveToFile.UseVisualStyleBackColor = true;
            // 
            // SchemaElements
            // 
            this.SchemaElements.Location = new System.Drawing.Point(409, 15);
            this.SchemaElements.Name = "SchemaElements";
            this.SchemaElements.Size = new System.Drawing.Size(146, 23);
            this.SchemaElements.TabIndex = 11;
            this.SchemaElements.Text = "Show Schema Elements";
            this.SchemaElements.UseVisualStyleBackColor = true;
            this.SchemaElements.Click += new System.EventHandler(this.SchemaElements_Click);
            // 
            // QueryUserName
            // 
            this.QueryUserName.Location = new System.Drawing.Point(80, 17);
            this.QueryUserName.Name = "QueryUserName";
            this.QueryUserName.Size = new System.Drawing.Size(180, 20);
            this.QueryUserName.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "UserName";
            // 
            // Results
            // 
            this.Results.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Results.Location = new System.Drawing.Point(24, 327);
            this.Results.Name = "Results";
            this.Results.Size = new System.Drawing.Size(620, 150);
            this.Results.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(25, 89);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 23);
            this.label6.TabIndex = 11;
            this.label6.Text = "Host";
            // 
            // Host
            // 
            this.Host.Location = new System.Drawing.Point(91, 86);
            this.Host.Name = "Host";
            this.Host.Size = new System.Drawing.Size(553, 20);
            this.Host.TabIndex = 12;
            this.Host.Text = "10.190.141.31";
            // 
            // MainForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(712, 508);
            this.Controls.Add(this.Host);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Results);
            this.Controls.Add(this.Tabs);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.LdapString);
            this.Controls.Add(this.label1);
            this.Name = "MainForm";
            this.Text = "Active Directory Query";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.Tabs.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Results)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.Run(new MainForm());
        }

        private void Form1_Load(object sender, System.EventArgs e)
        {

        }

        private void LoggedIn_CheckedChanged(object sender, System.EventArgs e)
        {
            if (this.LoggedIn.Checked)
            {
                this.User.Enabled = false;
                this.Pwd.Enabled = false;
            }
            else
            {
                this.User.Enabled = true;
                this.Pwd.Enabled = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Dictionary<Fields, string> GetFields()
        {
            var result = new Dictionary<Fields, string>();

            result.Add(Fields.sAMAccountName, "sAMAccountName");
            result.Add(Fields.CommonName, "cn");

            if (this.DisplayName.Checked)
            {
                result.Add(Fields.DisplayName, "displayName");
            }
            if (this.Email.Checked)
            {
                result.Add(Fields.Email, "mail");
            }
            if (this.GUID.Checked)
            {
                result.Add(Fields.Guid, "objectGUID");
            }
            if (this.Custom1.Checked)
            {
                result.Add(Fields.Custom1, this.CustomName1.Text);
            }
            if (this.Custom2.Checked)
            {
                result.Add(Fields.Custom2, this.CustomName2.Text);
            }


            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Test_Click(object sender, System.EventArgs e)
        {
            var button = sender as Button;
            this.Cursor = Cursors.WaitCursor;
            try
            {
                button.Enabled = false;

                if (this.LoggedIn.Checked)
                {
                    this.Results.DataSource = controller.LoggedinQuery(Host.Text, Filter.Text, LdapString.Text);
                }
                else
                {
                    this.Results.DataSource = controller.CustomCredentialQuery(Host.Text, LdapString.Text, Filter.Text, User.Text, Pwd.Text, DomainName.Text);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (button != null)
                {
                    button.Enabled = true;
                }
                this.Cursor = Cursors.Default;
            }

        }

        private void GetSchema_Click(object sender, EventArgs e)
        {
            this.Results.DataSource = this.controller.GetUserData(this.QueryUserName.Text, this.LdapString.Text);
        }

        private void SchemaElements_Click(object sender, EventArgs e)
        {
            this.Results.DataSource = this.controller.GetUserProperties(this.QueryUserName.Text, this.LdapString.Text);
        }

        public void ValidationFailure(string message)
        {
            MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
