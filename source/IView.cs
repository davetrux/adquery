﻿//Copyright (c) 2012 David Truxall
using System.Collections.Generic;

namespace AD_Query
{
    public interface IView
    {
        void ValidationFailure(string message);

        Dictionary<Fields, string> GetFields();
    }
}
