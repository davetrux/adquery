﻿//Copyright (c) 2012 David Truxall

namespace AD_Query
{
    public class UserData
    {
        protected string propertyName;
        protected string propertyType;
        protected string propertyValue;

        public string PropertyName
        {
            get
            {
                return this.propertyName;
            }
            set
            {
                this.propertyName = value;
            }
        }

        public string PropertyType
        {
            get
            {
                return this.propertyType;
            }
            set
            {
                this.propertyType = value;
            }
        }

        public string PropertyValue
        {
            get
            {
                return this.propertyValue;
            }
            set
            {
                this.propertyValue = value;
            }
        }
    }
}
