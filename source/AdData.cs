﻿//Copyright (c) 2012 David Truxall
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.DirectoryServices;
using System.Reflection;
using System.Text;

namespace AD_Query
{
    public enum Fields
    {
        sAMAccountName,
        CommonName,
        DisplayName,
        Guid,
        Email,
        Custom1,
        Custom2
    }

    public class AdData : IDirectory
    {
        protected string _ldapPath;
        protected string _ldapUser;
        protected string _ldapPassword;

        public AdData(string ldapConnection)
        {
            this._ldapPath = ldapConnection;
        }

        public AdData(string ldapConnection, string user, string password)
        {
            this._ldapPath = ldapConnection;
            _ldapUser = user;
            _ldapPassword = password;
        }


        protected DirectoryEntry GetEntry()
        {
            DirectoryEntry searchRoot = new DirectoryEntry(this._ldapPath, null, null, AuthenticationTypes.Secure);

            return searchRoot;
        }

        protected DirectorySearcher GetSearcher(string userName)
        {
            using (DirectoryEntry searcher = this.GetEntry())
            {
                DirectorySearcher ds = new DirectorySearcher(searcher, string.Format("(sAMAccountName={0})", userName));

                return ds;
            }
        }

        protected SearchResult GetSearchResult(string userName)
        {
            SearchResult sr = null;
            using (DirectorySearcher ds = this.GetSearcher(userName))
            {
                ds.SizeLimit = 1;

                using (SearchResultCollection src = ds.FindAll())
                {
                    if (src.Count > 0)
                        sr = src[0];
                }
            }

            return sr;
        }


        public List<ADProperties> GetUserProperties(string userName)
        {
            SearchResult sr = this.GetSearchResult(userName);
            List<ADProperties> props = new List<ADProperties>();
            ADProperties current = null;

            using (DirectoryEntry user = sr.GetDirectoryEntry())
            {
                using (DirectoryEntry schema = user.SchemaEntry)
                {
                    Type t = schema.NativeObject.GetType();

                    object optional = t.InvokeMember(
                        "OptionalProperties",
                        BindingFlags.Public | BindingFlags.GetProperty,
                        null,
                        schema.NativeObject,
                        null
                        );

                    if (optional is ICollection)
                    {
                        foreach (string s in ((ICollection)optional))
                        {
                            current = new ADProperties();
                            current.Name = s;
                            current.IsRequired = false;
                            props.Add(current);
                        }
                    }

                    object mand = t.InvokeMember(
                        "MandatoryProperties",
                        BindingFlags.Public | BindingFlags.GetProperty,
                        null,
                        schema.NativeObject,
                        null
                        );

                    if (mand is ICollection)
                    {
                        foreach (string s in ((ICollection)mand))
                        {
                            current = new ADProperties();
                            current.Name = s;
                            current.IsRequired = true;
                            props.Add(current);
                        }
                    }
                }
            }
            return props;
        }

        public List<UserData> GetUserData(string userName)
        {
            List<UserData> properties = new List<UserData>();

            SearchResult sr = this.GetSearchResult(userName);

            if (sr != null)
            {
                using (DirectoryEntry user = sr.GetDirectoryEntry())
                {
                    NameValueCollection props = new NameValueCollection();

                    foreach (string property in user.Properties.PropertyNames)
                    {
                        //sb.AppendLine(string.Format("{0} = {1}", property, user.Properties[property].Value.GetType().Name));
                        //sb.AppendLine(string.Format("{0} = {1}", property, user.Properties[property].Value));
                        UserData lineItem = new UserData();
                        lineItem.PropertyName = property;
                        lineItem.PropertyType = user.Properties[property].Value.GetType().Name;
                        lineItem.PropertyValue = user.Properties[property].Value.ToString();
                        properties.Add(lineItem);

                    }
                }
            }

            return properties;
        }

        protected string ByteArrayToString(byte[] byteArray)
        {
            int i = 0;
            StringBuilder outputString = new StringBuilder(byteArray.Length);
            for (i = 0; i < byteArray.Length; i++)
            {
                outputString.Append(byteArray[i].ToString("X2"));
            }
            return outputString.ToString();
        }

        public DataTable GetFromADLoggedIn(string filter, Dictionary<Fields, string> features)
        {
            using (DirectoryEntry directoryEntry = new DirectoryEntry(this._ldapPath))
            {
                using (DirectorySearcher directorySearcher = new DirectorySearcher(directoryEntry))
                {
                    return this.GetADData(directorySearcher, filter, features);
                }
            }
        }

        private DataTable GetADData(DirectorySearcher dsr, string filter, Dictionary<Fields, string> features)
        {
            SearchResultCollection searchResults = null;

            var dt = new DataTable();

            dsr.PropertiesToLoad.Add("cn");
            dsr.PropertiesToLoad.Add("SAMAccountName");
            dt.Columns.Add("cn");
            dt.Columns.Add("SAMAccountName");

            if (features.ContainsKey(Fields.DisplayName))
            {
                dsr.PropertiesToLoad.Add("displayName");
                dt.Columns.Add("displayName");
            }

            if (features.ContainsKey(Fields.Guid))
            {
                dsr.PropertiesToLoad.Add("ObjectGuid");
                dt.Columns.Add("GUID");
            }

            if (features.ContainsKey(Fields.Email))
            {
                dsr.PropertiesToLoad.Add("mail");
                dt.Columns.Add("mail");
            }

            if (features.ContainsKey(Fields.Custom1))
            {
                string fieldName;

                if (features.TryGetValue(Fields.Custom1, out fieldName))
                {
                    dsr.PropertiesToLoad.Add(fieldName);
                    dt.Columns.Add(fieldName);
                }
                else
                {
                    //error condition
                }
            }

            if (features.ContainsKey(Fields.Custom2))
            {
                string fieldName;
                if (features.TryGetValue(Fields.Custom2, out fieldName))
                {
                    dsr.PropertiesToLoad.Add(fieldName);
                    dt.Columns.Add(fieldName);
                }
                else
                {
                    //error condition
                }
            }

            dsr.Filter = filter;
            //dt.Columns.Add("cn");	

            searchResults = dsr.FindAll();

            string objectGuid;

            foreach (SearchResult sr in searchResults)
            {
                DataRow dr = dt.NewRow();

                dr["cn"] = CheckNull(sr.Properties["cn"][0].ToString());

                dr["SAMAccountName"] = sr.Properties["SAMAccountName"][0].ToString();

                if (features.ContainsKey(Fields.DisplayName))
                {
                    if (sr.Properties["displayName"] != null && sr.Properties["displayName"].Count > 0)
                    {
                        dr["displayName"] = sr.Properties["displayName"][0].ToString();
                    }
                }

                if (features.ContainsKey(Fields.Email))
                {
                    if (sr.Properties["mail"] != null && sr.Properties["mail"].Count > 0)
                    {
                        dr["mail"] = sr.Properties["mail"][0].ToString();
                    }
                }

                if (features.ContainsKey(Fields.Guid))
                {
                    objectGuid = ByteArrayToString((byte[])sr.Properties["ObjectGuid"][0]);
                    dr["GUID"] = objectGuid;
                }

                if (features.ContainsKey(Fields.Custom1))
                {
                    string fieldName;
                    if (features.TryGetValue(Fields.Custom2, out fieldName))
                    {
                        if (sr.Properties[fieldName].Count > 0)
                        {
                            dr[fieldName] = sr.Properties[fieldName][0].ToString();
                        }
                    }
                }

                if (features.ContainsKey(Fields.Custom2))
                {
                    string fieldName;
                    if (features.TryGetValue(Fields.Custom2, out fieldName))
                    {
                        if (sr.Properties[fieldName].Count > 0)
                        {
                            dr[fieldName] = sr.Properties[fieldName][0].ToString();
                        }
                    }
                }
                dt.Rows.Add(dr);
            }

            return dt;
        }

        public DataTable GetFromADCustom(string filter, Dictionary<Fields, string> features)
        {
            using (DirectoryEntry directoryEntry = new DirectoryEntry(this._ldapPath, this._ldapUser, this._ldapPassword))
            {
                using (DirectorySearcher directorySearcher = new DirectorySearcher(directoryEntry))
                {
                    return this.GetADData(directorySearcher, filter, features);
                }
            }
        }



        private string CheckNull(string info)
        {
            if (info == null)
                return "";
            else
                return info;
        }
    }
}
