﻿using System.Collections.Generic;
using System.Data;

namespace AD_Query
{
    public interface IDirectory
    {
        List<ADProperties> GetUserProperties(string userName);
        List<UserData> GetUserData(string userName);
        DataTable GetFromADLoggedIn(string filter, Dictionary<Fields, string> features);
        DataTable GetFromADCustom(string filter, Dictionary<Fields, string> features);
    }
}
