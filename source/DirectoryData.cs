﻿using System;
using System.Collections.Generic;
using System.Data;
using System.DirectoryServices.Protocols;
using System.Net;
using System.Text;
using System.Linq;
using System.Windows.Forms;

namespace AD_Query
{
    public class DirectoryData
    {
        private readonly string _ldapHost;
        private readonly string _ldapUser;
        private readonly string _ldapPassword;
        private readonly string _domain;

        public DirectoryData(string hostName)
        {
            _ldapHost = hostName;
        }

        public DirectoryData(string hostName, string user, string password, string domain)
        {
            _ldapHost = hostName;
            _ldapUser = user;
            _ldapPassword = password;
            _domain = domain;
        }
        public List<ADProperties> GetUserProperties(string userName)
        {
            throw new NotImplementedException();

        }

        public List<UserData> GetUserData(string userName)
        {
            throw new NotImplementedException();
        }

        public DataTable GetFromAdLoggedIn(string objectFilter, string ldapQuery, Dictionary<Fields, string> features)
        {
            var connection = new LdapConnection(_ldapHost);

            var atts = new List<string>(); // { "cn", "sAMAccountName", "objectGUID" };

            var dt = new DataTable();

            foreach (var att in features.Values)
            {
                atts.Add(att);
                dt.Columns.Add(att);
            }

            if (!string.IsNullOrEmpty(_ldapUser))
            {
                var credential = new NetworkCredential(_ldapUser, _ldapPassword, _domain);
                connection.Credential = credential;
            }

            var searchRequest = new SearchRequest(ldapQuery, objectFilter, SearchScope.Subtree, atts.ToArray());

            // cast the returned directory response as a SearchResponse object
            var searchResponse = (SearchResponse)connection.SendRequest(searchRequest);

            Console.WriteLine("\r\nSearch Response Entries:{0}", searchResponse.Entries.Count);

            // enumerate the entries in the search response
            foreach (SearchResultEntry entry in searchResponse.Entries)
            {
                var row = dt.NewRow();

                Console.WriteLine("{0}:{1}",
                                  searchResponse.Entries.IndexOf(entry),
                                  entry.DistinguishedName);
                var attributes = entry.Attributes;

                foreach (DirectoryAttribute attribute in attributes.Values)
                {
                    switch (attribute.Name)
                    {
                        case "objectGUID":
                            row[attribute.Name] = ByteArrayToString((byte[])attribute[0]);
                            break;
                        default:
                            row[attribute.Name] = attribute[0];
                            break;
                    }
                }
                dt.Rows.Add(row);
            }

            return dt;

        }

        private string CheckNull(string info)
        {
            if (info == null)
                return "";
            else
                return info;
        }

        protected string ByteArrayToString(byte[] byteArray)
        {
            var i = 0;
            var outputString = new StringBuilder(byteArray.Length);
            for (i = 0; i < byteArray.Length; i++)
            {
                outputString.Append(byteArray[i].ToString("X2"));
            }
            return outputString.ToString();
        }
    }
}
