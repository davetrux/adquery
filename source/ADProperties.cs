﻿//Copyright (c) 2012 David Truxall

namespace AD_Query
{
    public class ADProperties
    {
        protected string _name;
        public string Name
        {
            get
            {
                return this._name;
            }
            set
            {
                this._name = value;
            }
        }

        protected bool _isRequired;
        public bool IsRequired
        {
            get
            {
                return this._isRequired;
            }
            set
            {
                this._isRequired = value;
            }
        }
    }
}
