﻿//Copyright (c) 2012 David Truxall
using System;
using System.Collections.Generic;
using System.Data;

namespace AD_Query
{
    public class Controller
    {
        protected IView _form;

        public Controller(IView form)
        {
            this._form = form;
        }

        private bool ValidateQuery(string ldapPath, string filter)
        {
            if (string.IsNullOrEmpty(ldapPath))
            {
                this._form.ValidationFailure("Please enter an LDAP path");
                return false;
            }
            if (string.IsNullOrEmpty(filter))
            {
                this._form.ValidationFailure("Please enter an filter string");
                return false;
            }
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ldapPath"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public DataTable LoggedinQuery(string ldapHost, string filter, string ldapQuery)
        {
            try
            {
                if (this.ValidateQuery(ldapHost, filter))
                {
                    var data = new DirectoryData(ldapHost);

                    //AdData data = new AdData(ldapPath);

                    Dictionary<Fields, string> fieldList = this._form.GetFields();

                    return data.GetFromAdLoggedIn(filter, ldapQuery, fieldList);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                _form.ValidationFailure(ex.Message);
                return null;
            }
        }

        public DataTable CustomCredentialQuery(string ldapHost, string ldapQuery, string filter, string userName, string password, string domain)
        {
            try
            {
                if (this.ValidateQuery(ldapHost, filter))
                {
                    var data = new DirectoryData(ldapHost, userName, password, domain);

                    var fieldList = _form.GetFields();

                    return data.GetFromAdLoggedIn(filter, ldapQuery, fieldList);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                _form.ValidationFailure(ex.Message);
                return null;
            }
        }

        public List<UserData> GetUserData(string userName, string ldapPath)
        {
            if (!this.ValidateSchemaRequest(userName, ldapPath))
            {
                return null;
            }

            try
            {
                AdData data = new AdData(ldapPath);

                List<UserData> userInfo = data.GetUserData(userName);

                return userInfo;
            }
            catch (Exception ex)
            {
                this._form.ValidationFailure(ex.Message);
                return null;
            }
        }

        private bool ValidateSchemaRequest(string userName, string ldapPath)
        {
            if (string.IsNullOrEmpty(userName))
            {
                this._form.ValidationFailure("Please enter a user name");
                return false;
            }
            if (string.IsNullOrEmpty(ldapPath))
            {
                this._form.ValidationFailure("Please enter an LDAP string");
                return false;
            }
            return true;
        }

        public List<ADProperties> GetUserProperties(string userName, string ldapPath)
        {
            if (!this.ValidateSchemaRequest(userName, ldapPath))
            {
                return null;
            }
            try
            {
                AdData data = new AdData(ldapPath);

                List<ADProperties> userInfo = data.GetUserProperties(userName);

                return userInfo;
            }
            catch (Exception ex)
            {
                this._form.ValidationFailure(ex.Message);
                return null;
            }
        }
    }
}
